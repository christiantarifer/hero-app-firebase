import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// *************************************************************************************** //

import { delay, map } from 'rxjs/operators';

// *************************************************************************************** //

import { HeroeModel } from '../models/heroe.model';
import { Observable } from 'rxjs';

// *************************************************************************************** //
@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private url = 'https://login-app-1fc80.firebaseio.com';

  constructor( private http: HttpClient ) {

  }

  crearHeroe( heroe: HeroeModel ): Observable<any>{

    return this.http.post( `${this.url}/heroes.json`, heroe)
                .pipe(
                  map( (resp: any)  => {

                    heroe.id = resp.name;

                    return heroe;

                  })
                );

  }

  actualizarHeroe( heroe: HeroeModel ): Observable <any>{

    const heroeTemp = {

      ...heroe

    };

    delete heroeTemp.id;

    return this.http.put( `${ this.url}/heroes/${heroe.id}.json`, heroeTemp  );

  }

  borrarHeroe( id: string ): Observable <any>{

    return this.http.delete( `${this.url}/heroes/${id}.json` );

  }

  getHeroe( id: string ): Observable <any>{

    return this.http.get( `${this.url}/heroes/${id}.json` );

  }

  getHeroes(): Observable <any>{

    return this.http.get( `${this.url}/heroes.json` )
          .pipe(
            map( resp => this.crearArreglo(resp)  ),
            delay(1500)
          );

  }

  private crearArreglo( heroesObj: object ) {

    const heroes: HeroeModel[] = [];

    if ( heroesObj === null ) { return []; }

    Object.keys( heroesObj ).forEach(

      key => {

        const heroe: HeroeModel = heroesObj[key];

        heroe.id = key;

        heroes.push( heroe );

      }

    );

    return heroes;

  }

}
